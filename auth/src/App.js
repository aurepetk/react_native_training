import React, { Component } from 'react';
import { View } from 'react-native';
import firebase from 'firebase';
import { Header } from './components/common';
import LoginForm from './components/LoginForm';

class App extends Component {
    componentWillMount() {
        firebase.initializeApp({
            apiKey: 'AIzaSyByaiz0bHM3jD8tU2pyv9doft_LIl15vWY',
            authDomain: 'auth-afcd7.firebaseapp.com',
            databaseURL: 'https://auth-afcd7.firebaseio.com',
            storageBucket: 'auth-afcd7.appspot.com',
            messagingSenderId: '263032520829'
        });
    };

    render () {
        return (
            <View>
                <Header headerText="Authentication" />
                <LoginForm />
            </View>
        );
    };
}

export default App;
